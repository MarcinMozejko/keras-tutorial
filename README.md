# Advanced Keras tutorial

To run notebook be sure that you have `Docker` installed and run:

    make run-notebook

In your browser get to `http://localhost:8888/tree` in order to get to notebooks directory.
